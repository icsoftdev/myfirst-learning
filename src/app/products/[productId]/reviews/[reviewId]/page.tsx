import React from 'react'

const ReviewDetails = ({params}:{
    params:{
        productId:string
        reviewId:string
    }
}) => {
  return (
    <div>Review Id:{params.reviewId} of Product Id:{params.productId}</div>
  )
}

export default ReviewDetails